
import shuffle from '../utilities/utilities.js';
import Utilidades from '../utilities/utilidades.js';


export const EQUIPO_LOCAL = 0;
export const EQUIPO_VISTANTE = 1;

export default class WorldCup {

    constructor(name, equipos = [], config = {}) {
        this.name = name;
        this.setup(config);
        this.setupEquipos(equipos);
        this.grupos = [];
        this.nombreGrupos = [
            'Grupo A',
            'Grupo B',
            'Grupo C',
            'Grupo D',
            'Grupo E',
            'Grupo F',
            'Grupo G',
            'Grupo H'
        ];
    };

    setup(config) {
        const defaultConfig = {
            vueltas: 1,
            numeroEquiposPorGrupos: 4
        };
        this.config = Object.assign(defaultConfig, config);
    }


    setupGrupos() {
        const nEquiposGrupo = this.config.numeroEquiposPorGrupos;
        //const nombreEquipos = this.getNombresEquipos(this.equipos);
        const numeroGrupos = this.equipos.length / nEquiposGrupo; //8

        let inicio = 0;
        let fin = nEquiposGrupo;

        for (let i = 0; i < numeroGrupos; i++) {
            const grupo = {
                nombre: undefined,
                equiposGrupo: [],
                planificacionJornada: [],
                resumenes: [],
            }
            grupo.nombre = this.nombreGrupos[i];
            grupo.equiposGrupo = this.equipos.slice(inicio, fin);
            inicio = inicio + nEquiposGrupo;
            fin = fin + nEquiposGrupo;
            this.grupos.push(grupo);
        }
    }

    mostrarDetalleGrupo() {
        this.grupos.forEach(grupo => {
            console.log('\n' + grupo.nombre);
            console.log('-----------------------------');
            grupo.equiposGrupo.forEach(equipo => {
                console.log(equipo.name);
            })
            let i = 1;
            grupo.planificacionJornada.forEach(jornada => {
                console.log(`\nJornada ${i}:`);
                jornada.forEach(partido => {
                    console.log(`- ${partido[EQUIPO_LOCAL]} vs ${partido[EQUIPO_VISTANTE]}`);

                })
                i++;
            })
        })
    }

    /******************************************************************************************
      Modelo de equipo que queremos almacenar
         {
             name: 'España',
             partidosGanados: 0,
             partidosEmpatados: 0,
             partidosPerdidos:0
         }
        Esto es lo que vamos a crear por cada equipo en setupEquipos
        Esta configuración se guarda en un array que en cada posición guarda la 
        configuración de cada equipo.
    *******************************************************************************************/

    setupEquipos(nombreEquipos) {

        this.equipos = []
        for (const nombreEquipo of nombreEquipos) {
            const team = this.personalizarEquipo(nombreEquipo)
            this.equipos.push(team)
        }

        this.equipos.shuffle();
    }

    personalizarEquipo(nombreEquipo) {
        return {
            name: nombreEquipo,
            partidosGanados: 0,
            partidosEmpatados: 0,
            partidosPerdidos: 0
        }
    }

    //Obtenemos lo nombres de los equipos
    getNombresEquipos(equipos) {
        return equipos.map(equipo => equipo.name)
    }

    //Planificación de la liga.

    iniciarPlanificacion(equipos, planificacionJornada) {

        const numeroJornadas = equipos.length - 1; //Número de equipos - 1
        const numeroPartidosPorDia = equipos.length / 2;
        //Generamos partidos y partidos de la jornada
        for (let i = 0; i < numeroJornadas; i++) {
            const jornada = [] //Jornada vacía
            for (let j = 0; j < numeroPartidosPorDia; j++) {
                const partido = ['Equipo local', 'Equipo visitante']; //Partido
                jornada.push(partido);
            }
            //Una vez añadidos todos los partidos a la jornada, 
            //añadimos la jornada a la planificacionJornada          
            planificacionJornada.push(jornada); //Añadimos la jornada a la planificación          
        }
    }

    setEquiposLocales(equipos, planificacionJornada) {
        //guardamos los nombres de los equipos en un array nuevo
        const nombresEquipos = this.getNombresEquipos(equipos);

        //['A','B','C','D'] 0 1 2 3 para entender lo del -2
        const maxEquiposLocales = equipos.length - 2;
        let indiceEquipo = 0;

        planificacionJornada.forEach(jornada => { //Por cada jornada
            jornada.forEach(partido => { //Por cada partido
                //Establecemos el equipo local
                partido[EQUIPO_LOCAL] = nombresEquipos[indiceEquipo];
                indiceEquipo++;
                if (indiceEquipo > maxEquiposLocales) {
                    indiceEquipo = 0;
                }
            })
        })
    }

    //Establecemos todos los equipos visitantes menos el primero.
    setEquipoVisitante(equipos, planificacionJornada) {
        //guardamos los nombres de los equipos en un array nuevo
        const nombresEquipos = this.getNombresEquipos(equipos);
        //['A','B','C','D'] 0 1 2 3 para entender lo del -2
        const maxEquiposVisitantes = equipos.length - 2;
        let indiceEquipo = maxEquiposVisitantes;

        planificacionJornada.forEach(jornada => { //Por cada jornada
            let esElPrimerPartido = true;

            jornada.forEach(partido => { //Por cada partido
                // ******* Establecemos el equipo visitante
                //tenemos que saltanos el primer partico de cada jornada.
                if (esElPrimerPartido) {
                    esElPrimerPartido = false;
                }
                else {
                    partido[EQUIPO_VISTANTE] = nombresEquipos[indiceEquipo];
                    indiceEquipo--;
                    if (indiceEquipo < 0) {
                        indiceEquipo = maxEquiposVisitantes;
                    }
                }
            })
        })
    }

    //El ultimo equipo en las jornadas pares juega como local,
    //como el primer indice del array es 0, las jornadas pares seran la 1, 3....
    fixUltimoEquipoEnPlanificacionv1(equipos, planificacionJornada) {
        //guardamos los nombres de los equipos en un array nuevo
        const nombresEquipos = this.getNombresEquipos(equipos);

        const indiceEquipo = equipos.length - 1;

        const ultimoEquipo = nombresEquipos[indiceEquipo];
        //Recorremos todas las jornadas que es planificacionJornada
        for (let i = 0; i < planificacionJornada.length; i++) {
            const partido = planificacionJornada[i];
            const primerPartido = partido[0];
            if (i % 2 == 0) { //Si el indice de la jornada es par -> Juega fuera de casa          
                primerPartido[EQUIPO_VISTANTE] = ultimoEquipo;
            } else { //Si el indice de la  jornada es impar juega en casa
                primerPartido[EQUIPO_VISTANTE] = primerPartido[EQUIPO_LOCAL];
                primerPartido[EQUIPO_LOCAL] = ultimoEquipo;
            }
        }
    }

    planificarJornadas(equipos, planificacionJornada) {
        // https://es.wikipedia.org/wiki/Sistema_de_todos_contra_todos
        this.iniciarPlanificacion(equipos, planificacionJornada);
        this.setEquiposLocales(equipos, planificacionJornada);
        this.setEquipoVisitante(equipos, planificacionJornada);
        this.fixUltimoEquipoEnPlanificacionv1(equipos, planificacionJornada);
    }

    iniciarPlanificacionesGrupos() {
        this.grupos.forEach(grupo => {
            this.planificarJornadas(grupo.equiposGrupo, grupo.planificacionJornada);
        })

    }


    iniciar() {
        let indiceGrupos = 0;
        let indiceJornada = 0;
        const numeroDeGrupos = this.grupos.length;

        while (indiceGrupos < numeroDeGrupos) {

            const resumenJornada = {
                resultados: [],
                clasificacion: undefined,
            };
            const grupo = this.grupos[indiceGrupos];
            const jornadas = grupo.planificacionJornada;
            const numeroJornadas = jornadas.length - 1;
            const jornada = grupo.planificacionJornada[indiceJornada];
            
            jornada.forEach(partido => {
                const utilidades = new Utilidades();
                const resultado = utilidades.jugar(partido);
                this.actualizarEquipos(grupo.equiposGrupo, resultado); //Actualizamos los equipos con el resultado del partido.
                resumenJornada.resultados.push(resultado);//Guardamos el resumen de la jornada.
            })
            // Calcular clasificación
            this.getClasificacion(grupo.equiposGrupo, this.grupos[indiceGrupos].resumenes);
            resumenJornada.clasificacion = grupo.equiposGrupo.map(equipo => Object.assign({}, equipo));
            grupo.resumenes.push(resumenJornada); //Guardamos los resumenes de las jornadas.
            indiceGrupos++;
            if ((indiceGrupos == numeroDeGrupos) &&
                (indiceJornada != numeroJornadas)) {
                indiceGrupos = 0;
                indiceJornada++;
            }
        }
    }//Fin iniciar()

     //Sacar los equipos que pasan de ronda
     getEquiposPasanRonda() {
        const ganadores = [];
        const segundos = [];
        let i = 0;

        this.grupos.forEach(grupo => { //Recorremos los grupos
            const numeroJornadas = grupo.planificacionJornada.length - 1;
            const clasificacionFinalGrupo = grupo.resumenes[numeroJornadas].clasificacion;
            ganadores.push(clasificacionFinalGrupo[0]);
            segundos.push(clasificacionFinalGrupo[1]);
            i++;
        })
        const enfrentamientos1 = [];
        const enfrentamientos2 =[];
        let indice = 0;
        while(indice < ganadores.length){
            if(indice%2 == 0){
                const winner = ganadores[indice];
                const second = segundos[indice+1];
                enfrentamientos1.push(winner);
                enfrentamientos1.push(second);
            }else{
                const winner = ganadores[indice];
                const second = segundos[indice-1];
                enfrentamientos2.push(winner);
                enfrentamientos2.push(second);
            }
            indice++;
        }
        return [
            ...enfrentamientos1,
            ...enfrentamientos2,
        ]
    }//Fin getEquiposPasanRonda()

    jugar(partido) {
        throw new Error('Método jugar no implementado');
    }

    actualizarEquipos(resultado) {
        throw new Error('Método actualizarEquipos no implementado');
    }

    getClasificacion() {
        throw new Error('Método getClasificación no implementado');
    }

}