import WorldCup from './mundial.js';


export default class LigaPorPuntos extends WorldCup {
    constructor(name, equipos = [], config = {}) {
        super(name, equipos, config);
    }

    setup(config) {
        const defaultConfig = {
            vueltas: 1,
            puntosGanar: 3,
            puntosEmpatar: 1,
            puntosPalmar: 0,
            numeroEquiposPorGrupos: 4
        }
        this.config = Object.assign(defaultConfig, config);
    }

    /********************************************************************************************
    * Metodo que llama al método personalizarEquipo del padre                                  *
    * Devuelve un objeto con los mismo datos del objeto que duvuelve el padre y ademas         *
    * las propiedades puntos, golesFavor y golesContra.                                        *
    * Para hacerlo usamos SPREADING                                                            *
    ********************************************************************************************/
    personalizarEquipo(nombreEquipo) {
        const personalizarEquipo = super.personalizarEquipo(nombreEquipo);
        return {
            puntos: 0,
            golesFavor: 0,
            golesContra: 0,
            ...personalizarEquipo
        }
    }

    //Busca el equipo por su nombre en el array de equipos
    getEquipoPorNombre(equipos, nombreEquipo) {
        return this.equipos.find(equipo => equipo.name == nombreEquipo);
    }

    //Método que actualiza los equipos en función del resultado del encuentro.
    actualizarEquipos(equipos, resultado) {
        //Buscar el equipo por su nombre en el array de equipos
        const equipoLocal = this.getEquipoPorNombre(equipos, resultado.equipoLocal);
        const equipoVisitante = this.getEquipoPorNombre(equipos, resultado.equipoVisitante);
        if (equipoLocal && equipoVisitante) {
            equipoLocal.golesFavor = resultado.golesLocal;
            equipoLocal.golesContra = resultado.golesVisitante;
            equipoVisitante.golesFavor = resultado.golesVisitante;
            equipoVisitante.golesContra = resultado.golesLocal;
            if (resultado.golesLocal > resultado.golesVisitante) {
                equipoLocal.puntos += this.config.puntosGanar;
                equipoLocal.partidosGanados += 1;
                equipoVisitante.puntos += this.config.puntosPalmar;
                equipoVisitante.partidosPerdidos += 1;
            } else if (resultado.golesLocal < resultado.golesVisitante) {
                equipoLocal.puntos += this.config.puntosPalmar;
                equipoLocal.partidosPerdidos += 1;
                equipoVisitante.puntos += this.config.puntosGanar;
                equipoVisitante.partidosGanados += 1;
            } else {
                equipoLocal.puntos += this.config.puntosEmpatar;
                equipoLocal.partidosEmpatados += 1;
                equipoVisitante.puntos += this.config.puntosEmpatar;
                equipoVisitante.partidosEmpatados += 1;
            } //Empatan
        }
    }

    //Creamos la clasificación de cada jornada
    getClasificacion(equipos, resultados) {
        equipos.sort(function(equipoA, equipoB){
            if (equipoA.puntos > equipoB.puntos) {
                return -1 //izquierda va delante del de la derecha
            } else if (equipoA.puntos < equipoB.puntos) {
                return 1 //El de la derecha va delante del de la izquierda
            } else { //Hay empate a puntos entonces va por delante el que haya ganado el enfrentamiento directo
                //Buscar equipos para sacar resultados
                if (resultados.length != 0) {//Comprobamos si hay resultados
                    //Buscamos si esta el resultado entre ambos equipos.
                    //const partidoEncontrado = this.buscarPartido(resultados);
                    const partidoEncontrado = [];
                    for (let i = 0; i < resultados.length; i++) {
                        for (let j = 0; j < resultados[i].resultados.length; j++) {
                            let elemento = resultados[i].resultados[j];
                            if (((elemento.equipoLocal == equipoA.name) &&
                                (elemento.equipoVisitante == equipoB.name)) ||
                                ((elemento.equipoLocal == equipoB.name) &&
                                    (elemento.equipoVisitante == equipoA.name))) {
                                partidoEncontrado.push(elemento);
                            }
                        }
                    }
                    if (partidoEncontrado.length != 0) {
                        if (partidoEncontrado[0].golesLocal > partidoEncontrado[0].golesVisitante) {
                            return -1; //izquierda va delante del de la derecha
                        } else if (partidoEncontrado[0].golesLocal < partidoEncontrado[0].golesVisitante) {
                            return 1; // derecha va delande del de la izquierda
                        } else { //Han empatado el partido
                            //Hay empatan en el enfrentamiento directo por encima el que mas goles tenga
                            const diferenciaGolesA = equipoA.golesFavor - equipoA.golesContra;
                            const diferenciaGolesB = equipoB.golesFavor - equipoB.golesContra;
                            if (diferenciaGolesA > diferenciaGolesB) {
                                return -1
                            } else if (diferenciaGolesA < diferenciaGolesB) {
                                return 1
                            } else if (equipoA.name > equipoB.name) { //Hay empate en la diferencia de goles orden alfabético
                                return 1;
                            } else if (equipoA.name < equipoB.name) {
                                return -1;

                            } else {
                                return 0;
                            }
                        }
                    }
                }
            }
        })
    }//Fin getClasificacion()
}