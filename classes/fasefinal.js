
import WorldCup, { EQUIPO_LOCAL, EQUIPO_VISTANTE } from './mundial.js';

import Utilidades from '../utilities/utilidades.js';
import utilities from '../utilities/utilities.js';


export default class FaseFinal extends WorldCup {
    //Necesitamos los equipos que han pasado a esta fase

    constructor(name, equipos = [], config = {}) {
        super(name, equipos, config)

        this.fases = [];

        this.equiposFinalConsolacion = [];

        this.nombrefases = ['OCTAVOS DE FINAL',
            'CUARTOS DE FINAL',
            'SEMIFINALES',
            'TERCER Y CUARTO PUESTO',
            'FINAL'];

        this.setupEquipos(equipos);
    }

    /******************************************************************************************
      Modelo de equipo que queremos almacenar
         {
             name: 'España',
             partidosGanados: 0,
             partidosEmpatados: 0,
             partidosPerdidos:0
         }
        Esto es lo que vamos a crear por cada equipo en setupEquipos
        Esta configuración se guarda en un array que en cada posición guarda la 
        configuración de cada equipo.
    *******************************************************************************************/

    setupEquipos(nombreEquipos) {

        this.equipos = []
        for (const nombreEquipo of nombreEquipos) {
            const team = this.personalizarEquipo(nombreEquipo)
            this.equipos.push(team)
        }
    }

    /*****************************************************************
       En este caso los enfrentamientos son a partido uníco en 
       enfrentamiento directo entre dos equipos por lo que la 
       planificación de la jornada es diferente que en una liga.
       Ya que yo no se que equipos juegan la siguiente fase hasta que
       que se juegue el partido.
     *****************************************************************/

    iniciarPlanificarEnfrentamientos(equiposFase) {
        const numeroPartidos = equiposFase.length / 2;
        //Generamos los partidos
        const partidosFase = []; //Creamos una fase vacia
        //Generamos los partidos entre los equipos
        for (let i = 0; i < numeroPartidos; i++) {
            const partido = ['Equipo local', 'Equipo visitante']; //Partido
            partidosFase.push(partido);//Guardo los partidos en la fase
        }
        return partidosFase; //Mandamos a la función los encuentros generados para que los guarde en la fase
    }

    setEquiposPartido(planificacionFaseFinal, equipos) {
        const nombresEquipos = this.getNombresEquipos(equipos);
        const numeroEquipos = this.equipos.length;
        let indiceInicio = 0;
        let indiceFinal = 1;
        //Recorremos la planificación para ir configurando los equipos que juegan en la fase
        planificacionFaseFinal.forEach(partido => {
            partido[EQUIPO_LOCAL] = nombresEquipos[indiceInicio];
            partido[EQUIPO_VISTANTE] = nombresEquipos[indiceFinal];
            indiceInicio = indiceInicio + 2 ;
            indiceFinal = indiceFinal +2;

        });
    }

    jugarPartidos(fase) {
        let resultado = {};
        let resultadosEnfrentamientos = [];
        for (const partido of fase) {
            const utilidades = new Utilidades();
            do {
                resultado = utilidades.jugar(partido);
            } while (resultado.golesLocal == resultado.golesVisitante)
            //Si empatan generamos nuevo resultado
            resultadosEnfrentamientos.push(resultado);
        }
        return resultadosEnfrentamientos;
    }

    getEquiposPasan(nombreEquipo) {
        const equipo = this.equipos.find(equipo => equipo.name == nombreEquipo);
        return equipo;
    }

    setEquiposGanadores(nombreEquipo) {
        const numeroEquipos = this.equipos.length - 1;
        let indice = 0;
        let esta = false;
        while (esta == false) {
            if (this.equipos[indice].name == nombreEquipo) {
                esta = true;
            }
            indice++;
        }
        this.equipos.splice(indice, 1);
    }

    ganadoresEnfrentamientos(resultadosEnfrentamientos, eliminatoria) {
        let equipoGanador = ""
        let equipoPerdedor = "";
        const equiposPasan = [];

        resultadosEnfrentamientos.forEach(resultado => {
            if (resultado.golesLocal > resultado.golesVisitante) {
                equipoGanador = resultado.equipoLocal;
                equipoPerdedor = resultado.equipoVisitante;
            } else {
                equipoGanador = resultado.equipoVisitante;
                equipoPerdedor = resultado.equipoLocal;
            }
            if (eliminatoria == 'SEMIFINALES') {
                this.equiposFinalConsolacion.push(this.getEquiposPasan(equipoPerdedor));
            }
            equiposPasan.push(this.getEquiposPasan(equipoGanador));
        })
        return equiposPasan;
    }

    iniciarFaseFinal() {
        const numeroFases = this.nombrefases.length;
        for (let i = 0; i < numeroFases; i++) {
            const fase = {
                nombre: undefined,
                equiposFase: [],
                planificacionFaseFinal: [],
                resultadosEnfrentamientos: [],
            }
            fase.nombre = this.nombrefases[i];
            if (fase.nombre == 'TERCER Y CUARTO PUESTO') {
                fase.equiposFase = this.equiposFinalConsolacion;
            } else {
                fase.equiposFase = this.equipos;
            }
            fase.planificacionFaseFinal = this.iniciarPlanificarEnfrentamientos(fase.equiposFase);
            this.setEquiposPartido(fase.planificacionFaseFinal, fase.equiposFase);
            //Jugar los partidos
            fase.resultadosEnfrentamientos = this.jugarPartidos(fase.planificacionFaseFinal);
            //Guardar las fase en el array fases
            this.fases.push(fase);
            if (fase.nombre != 'TERCER Y CUARTO PUESTO') {
                this.equipos = this.ganadoresEnfrentamientos(fase.resultadosEnfrentamientos, fase.nombre);
            }
        }
    }
}