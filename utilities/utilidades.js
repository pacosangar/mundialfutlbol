import { EQUIPO_LOCAL, EQUIPO_VISTANTE } from "../classes/mundial.js";


export default class Utilidades{

	constructor(){
		

	}

	generarResultado() {
		return Math.round(Math.random() * 10);
	}

	jugar(partido){

		const golesLocal = this.generarResultado();
		const golesVisitante = this.generarResultado();
		return{
			equipoLocal: partido[EQUIPO_LOCAL], golesLocal,
			equipoVisitante: partido[EQUIPO_VISTANTE], golesVisitante,
		}
	}
}
	