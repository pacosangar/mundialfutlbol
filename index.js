import {mundialTeams} from './teams.js';

import FaseFinal  from './classes/fasefinal.js';

import ligaMundialFulbol from './classes/ligaPorPuntos.js'

const equiposMundial = mundialTeams;

const mundial = new ligaMundialFulbol('Mundial 2021', equiposMundial);

//setupEquipos, customiza, guarda en un array el objeto literal y mezcla.
mundial.setupEquipos(equiposMundial);

console.log('\nGrupos y equipos');

console.log('=============================');

mundial.setupGrupos();

mundial. iniciarPlanificacionesGrupos();

mundial.mostrarDetalleGrupo();

console.log('\n=========================================');
console.log('========== COMIENZA EL MUNDIAL ==========');
console.log('=========================================');

mundial.iniciar();

//Mostramos los resultados de las jornadas de todos los grupos y la clasificación de cada jornada.

let indiceGrupos = 0;
let indiceJornada = 0;
let indiceResumen = 0;
const numeroDeGrupos = mundial.grupos.length;

while (indiceGrupos < numeroDeGrupos) {

    const grupo = mundial.grupos[indiceGrupos];

    const jornadas = grupo.planificacionJornada;

    const numeroJornadas = jornadas.length - 1;

    console.log('\n' + grupo.nombre + ` - Jornada ${indiceJornada + 1}`);
    console.log('-------------------');
    
    //Tenemos que mostrar los resumenes de cada jornada y la clasificación
    
    const resumen = grupo.resumenes[indiceResumen];
    
    resumen.resultados.forEach(resultado=>{
        console.log(`${resultado.equipoLocal} ${resultado.golesLocal} - ${resultado.equipoVisitante} ${resultado.golesVisitante}`);
    })

    console.table(resumen.clasificacion.map(equipo =>{
        return {
            Equipo: equipo.name,
            Puntos: equipo.puntos,
            GolesFavor: equipo.golesFavor,
            GolesContra: equipo.golesContra,
            DiferenciaGoles: equipo.golesFavor - equipo.golesContra,
        }
    }));
    
    indiceGrupos++;
    if ((indiceGrupos == numeroDeGrupos) &&
        (indiceJornada != numeroJornadas)) {
        indiceGrupos = 0;
        indiceJornada++;
        indiceResumen++;
    }
}

const equiposPasanDeRonda = mundial.getEquiposPasanRonda();

//Fase final del mundial

console.log('\n============================================================');
console.log('==========  COMIENZO DE LA FASE DE ELIMINATORIAS  ==========');
console.log('============================================================');

const nombresEquipos = mundial.getNombresEquipos(equiposPasanDeRonda);

const faseFinal = new FaseFinal('Mundial 2021', nombresEquipos);

faseFinal.iniciarFaseFinal();

faseFinal.fases.forEach(fase=>{
    console.log('\n===== ' + fase.nombre + ' =====\n')
    let equipoGanador = ""
    fase.resultadosEnfrentamientos.forEach(resultado =>{
        if (resultado.golesLocal > resultado.golesVisitante) {
            equipoGanador = resultado.equipoLocal;
        } else {
            equipoGanador = resultado.equipoVisitante;
        }
        console.log(resultado.equipoLocal + ' ' +
            resultado.golesLocal + ' - ' +
            resultado.golesVisitante + ' ' +
            resultado.equipoVisitante + ' => ' +
            equipoGanador);
    })
})

faseFinal.equipos.forEach(equipo => {
    console.log('\n============================================================');
    console.log(`! ${equipo.name} campeón del mundo!`)
    console.log('============================================================');
});