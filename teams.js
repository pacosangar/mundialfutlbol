
export const europaTeams = [
                            'España', 
                            'Italia', 
                            'Alemania', 
                            'Francia', 
                            'Grecia',
                            'Inglaterra',
                            'Eslovenia', 
                            'Serbia', 
                            'Países Bajos', 
                            'Dinamarca', 
                            'Portugal', 
                            'Suiza', 
                            'Eslovaquia'
                        ];
export const asiaTeams = [
                            'Corea del Sur', 
                            'Corea del Norte', 
                            'Japón'
                        ];

export const africaTeams = [
                            'Nigeria', 
                            'Sudafrica', 
                            'Argelia', 
                            'Ghana', 
                            'Camerún', 
                            'Costa de Marfil'
                        ];
export const americaTeams = [
                                'Chile', 
                                'Argentia', 
                                'Brasil', 
                                'Uruguay', 
                                'Mexico', 
                                'Paraguay', 
                                'Honduras', 
                                'EEUU'
                            ];
export const oceaniaTeams = [
                                'Australia', 
                                'Nueva Zelanda'
                            ];

export const mundialTeams = [
                                ...europaTeams,
                                ...asiaTeams,
                                ...africaTeams,
                                ...americaTeams,
                                ...oceaniaTeams,
                            ];

